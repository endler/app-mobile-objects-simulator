package socket;

import java.io.InputStream;
import java.util.Scanner;
import org.json.simple.parser.ParseException;
import mhub_communication.CommunicationManager;

/*Handles communication for each client. Whenever something is sent by the mobile hub, this class will call the appropriate
 * communication manager. See CommunicationManager class for more details.*/


public class InputHandler implements Runnable {
	/*Parameters:
	 * input: InputStream of this handler, the mobile hub connection.
	 * manager: The CommunicationManager of this class, will be notified every time something is received in input. See class
	 * 	CommunicationManager for more details.*/
	private InputStream input;
	private CommunicationManager manager;
	
	
	public InputHandler(InputStream input, CommunicationManager myManager){
		this.input = input;
		this.manager = myManager;
		
	}
	
	public void run(){
		Scanner s = new Scanner(this.input);
	
		while(s.hasNextLine()){
			//System.out.println(s.nextLine());
			try {
				manager.handle(s.nextLine());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		s.close();
	}
	
}
