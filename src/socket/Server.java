package socket;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import mhub_communication.CommunicationManager;
import java.io.PrintStream;

/*Socket configuration class*/
public class Server {

	/*
	 * Parameters:
	 * server: ServerSocket that will be used to communicate with the mobile hub.
	 * */
	
	private ServerSocket server;
	
	public Server (int porta) throws IOException {
      
		server = new ServerSocket(porta); 
	     System.out.println("Porta " + porta + " aberta!");

	     
	   }
	//Accept client connection indefinitely
	 public void acceptClient() throws IOException {
		 while(true){
			 Socket client = server.accept();	
			 System.out.println("Nova conex�o com o cliente " +   client.getInetAddress().getHostAddress());
			 
			 PrintStream newExit = new PrintStream(client.getOutputStream());
			 CommunicationManager newManager = new CommunicationManager(newExit);
			 InputStream newIn = client.getInputStream();			 	  			   
			 
			 InputHandler newHub = new InputHandler(newIn,newManager);
			 new Thread(newHub).start();
			 
		 }
		   
	   }
	 
}
