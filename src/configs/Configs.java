package configs;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import mobj.MOBJ;
/*
 * MOBJ and Sensors configuration class.
 * */
public class Configs {
	
	/*Parameters:
	 * Lmobj: List of simulated mobile objects, as specified in Configs.txt.
	 * errorProbability: % chance of disconnecting the mobile object when the simulator receives a read request
	 * scanTime: interval between Mobile Objects data update. See class CommunicationManager for more details.
	 * lastTime: last current time, in seconds, that the values in Lmobj was updated. See class CommunicationManager for more details.
	 * smp: Semaphore of the configuration class.
	 * */
	
	private long errorProbability, scanTime, lastTime;
	private List<MOBJ> Lmobj;
	private Semaphore smp;
	private static Configs Instanceof = null;
	
	private Configs(){
		
		JSONParser parser = new JSONParser();
		//obj: txt file containing the JSON string configuration of the project
		Object obj;
		
		
		try {
			smp = new Semaphore(1);
			obj = parser.parse(new FileReader( "Configs.txt"));
			JSONObject jsonObject = (JSONObject) obj;
			errorProbability = (long) jsonObject.get("ErrorChance");
			scanTime = (long) jsonObject.get("Time");
			scanTime = scanTime*1000;
			lastTime = 0;
			JSONArray Set = (JSONArray) jsonObject.get("Set");
			
			int i;

			this.Lmobj = new ArrayList<MOBJ>();
			
			//	
			int k=0;
			Long nMobj, nSensor;
			Double lower, upper;
			String service_name, sensor_class;
			
			for(i=0; i<Set.size() ;i++){
				JSONObject temp = (JSONObject) Set.get(i);
	
				
				 nMobj = (Long) temp.get("MOBJS");
				 JSONArray lowerBoundArray = (JSONArray) temp.get("LowerBound");
				 JSONArray upperBoundArray = (JSONArray) temp.get("UpperBound");
				 JSONArray SensorArray = (JSONArray) temp.get("Sensor");
				 JSONArray ServiceNameArray = (JSONArray) temp.get("ServiceName");
				 JSONArray SensorClassArray = (JSONArray) temp.get("Classe");
				 int sizeArrays = lowerBoundArray.size();
				 JSONArray macList;
				 
				 try{
					 macList = (JSONArray) temp.get("MAC");
				 }catch(Exception e){
					 macList = new JSONArray();
				 }			 
				 
				 //nSensor = (Long) temp.get("Sensor");
				 //lower = (Double) temp.get("LowerBound");
				 //upper = (Double) temp.get("UpperBound");
				 //String service_name = (String) temp.get("ServiceName");
				 
				 for(int j = 0; j < nMobj; j++)
				 {	
					 
					 try{
						 this.Lmobj.add(new MOBJ( (String) macList.get(j)));	
					 }catch(Exception ex){
						 this.Lmobj.add(new MOBJ());
					 }
					 
					 for(int jj = 0; jj < sizeArrays; jj++)
					 {
						 nSensor = (Long) SensorArray.get(jj);
						 lower = (Double) lowerBoundArray.get(jj);
						 upper = (Double) upperBoundArray.get(jj);
						 service_name = (String) ServiceNameArray.get(jj);
						 sensor_class = (String) SensorClassArray.get(jj);
						 
						 this.Lmobj.get(k).AddNSensors(nSensor, lower, upper, service_name, sensor_class);
						 this.Lmobj.get(k).setRandomValuesALL();
					 }
					 
					// this.Lmobj.get(k).AddNSensors(nSensor, lower, upper, service_name);
					 //this.Lmobj.get(k).setRandomValuesALL();
					 k++;
				 }
				
			}		
						 								
		} 
								
		catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			//If an error occurs while opening Configs.txt, exit.
			System.exit(1);
		}
	
	}
	
	public static Configs getInstance(){
		if(Instanceof == null){
			Instanceof = new Configs();
		}
		
		return(Instanceof);
	}
	
	public List<MOBJ> getListMOBJ()
	{
		return(this.Lmobj);
	}
	
	public void acquireSemaphore() {
		try {
			this.smp.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void releaseSemaphore(){
		this.smp.release();
	}
	
	public long getChance(){
		return errorProbability;
	}
	
	public long getTime(){
		return scanTime;
	}
	
	public void setLastUpdate(long arg){
		this.lastTime = arg;
	}
	
	public long getLastUpdate(){
		return(this.lastTime);
	}
	
}
