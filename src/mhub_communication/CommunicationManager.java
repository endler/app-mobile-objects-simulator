package mhub_communication;


import java.io.PrintStream;
import java.util.Random;
import java.util.concurrent.Semaphore;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import configs.Configs;

/*Data handler class. Class that will handle all the data sent by the mobile hub, notified by the InputHandler.*/
public class CommunicationManager {
	/*
	 * Parameters:
	 * exit: PrintStream of the socket, that will be used to send data to the mobile hub.
	 * conf: The configuration parameters, specified in Configs.txt. See Configs class for more details.
	 * */

	private PrintStream exit;
	private Configs conf;
	private int currentPos;

	
	
	public CommunicationManager(PrintStream saida){
		 this.exit = saida;	 
		 this.conf = Configs.getInstance();		
		 currentPos = 0;
		
	}

	
	/*Method used to handle the data (JSON string) sent by the mobile hub*/
	@SuppressWarnings("unchecked")
	public void handle(String mensagem) throws ParseException{
			//System.out.println(mensagem);
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(mensagem);
      
         
			//command: The type of request sent by the mobile hub. It can be a read MOBJ values request, an initialize 
			//MOBJ values request, a destroy all the MOBJ request or a write values.
			String command = (String) jsonObject.get("Nome");
         
			
			//StartScan request. Called by the mobile hub when it wants to read all the values of the simulated MOBJ.
			//Randomizes all values, randomizes disconnection based on the errorProbability parameter of the Configs class.
			if(command.equals("StartScan"))
			{
				System.gc();

					//System.out.println("Recebi o StartScan!");	
					
					conf.acquireSemaphore();
					//If the last time the mobile objects values were updated is greater than the parameter conf.getTime, 
					//we generate new values for all mobile object's sensors.
					long diff = System.currentTimeMillis() - conf.getLastUpdate();
					if(diff >= conf.getTime())
					{
						conf.setLastUpdate(System.currentTimeMillis());
						for(int i = 0; i < this.conf.getListMOBJ().size(); i++)
						{
							conf.getListMOBJ().get(i).setRandomValuesALL(); 
						}
					}
					conf.releaseSemaphore();
					 
					JSONObject jason = new JSONObject();
					JSONArray listSensorValue = new JSONArray();
					JSONArray listServiceName = new JSONArray();
					JSONObject Intermediario = new JSONObject();
					
				
					for(int i = 0; i < this.conf.getListMOBJ().size(); i++)
					{
						
						for(int j = 0; j < this.conf.getListMOBJ().get(i).listSensorSize(); j++)
						{
							listSensorValue.add(this.conf.getListMOBJ().get(i).getSensorValue(j));
							listServiceName.add(this.conf.getListMOBJ().get(i).getSensorServiceName(j));
						}
						
						Intermediario.put("SensorValues", listSensorValue.clone());
						Intermediario.put("Nome", this.conf.getListMOBJ().get(i).getName());
						Intermediario.put("ServiceName", listServiceName.clone());
						
						Random r = new Random(); 
						int x = r.nextInt(100);
										
						if(x >= this.conf.getChance()){Intermediario.put("ConnectStatus", "Connected");}
						else{Intermediario.put("ConnectStatus", "Disconnected");}	
																
						Intermediario.put("Position", i);
						jason.put("Objetos", Intermediario.clone());					
						jason.put("Comando", "StartScanReply");
						exit.println(jason.toString());
						listSensorValue.clear();
						listServiceName.clear();
						Intermediario.clear();
						jason.clear();
					
					}				
			}	
			
			/*Initialize request. Called by the mobile hub when it wants to initialize the simulation process.
			 * The Mobile Objects list is built based on a stop and wait protocol, so the MHub initializes the request
			 * with a "Initialize" request. The simulator then adds a Monj to the list, and sends a reply to the MHub.
			 * The MHub adds the object to the list of created mobile objects and sends an Acknowledgement to the simulator,
			 * requesting the next object. If at any given moment the current object examined by the simulator is the last
			 * object in the config files, a boolean "Last?" will be set as true on the sent JSON string, finishing the 
			 * process of initializing the MObj list.
			 *  */
			else if(command.equals("Initialize") || command.equals("Acknowledged"))
			{		
				//System.out.println("Recebi o Initialize!");
				
				JSONObject jason = new JSONObject();
				JSONObject Intermediario = new JSONObject();
				JSONArray Services = new JSONArray();
			
				
				int size = this.conf.getListMOBJ().size();			
				
				Intermediario.put("Nome", this.conf.getListMOBJ().get(currentPos).getName());
				for(int i = 0; i < this.conf.getListMOBJ().get(currentPos).listSensorSize(); i++)
				{				
					String tempName = this.conf.getListMOBJ().get(currentPos).getSensorServiceName(i);						
					if(!Services.contains(tempName))
					{
						Services.add(tempName);
					}
					tempName = null;
					System.gc();						
				}
				
				Intermediario.put("ServiceName", Services);
				Intermediario.put("Position", currentPos);
					
				if(currentPos == size - 1)
				{
					Intermediario.put("Last?", true);
					currentPos = 0;
				}
				else
				{
					Intermediario.put("Last?", false);
					currentPos++;
				}
					
				jason.put("Objetos", Intermediario.clone());
				jason.put("Comando", "InitializeReply");				
				exit.println(jason.toString());
				jason.clear();
				Intermediario.clear();
					
			}
					
			/*Destroy request. Clears the MOBJs list*/
			else if(command.equals("Destroy"))
			{
				//System.out.println("Recebi o Destroy!");
				//this.Lmobj.clear();
			}
			
			/*Write request. Currently not implemented in the mobile hub. Writes a given value to the specified (endereco) MOBJ.*/
			else if(command.equals("WriteSensor"))
			{
				
				
				String endereco_0 = (String) jsonObject.get("macAdd");
				String endereco;
				if(endereco_0.substring(0, 2).equalsIgnoreCase("4-"))
				{
					 endereco = endereco_0.substring(2);
				}
				else
				{
					 endereco = endereco_0;
				}
				
				
				String temp_name = (String) jsonObject.get("sensorName");
				
				Double valor = (Double) jsonObject.get("sensorValue");
				
				System.out.println("Write Request on " + endereco + ", Service: " + temp_name + ", Value: " + valor);
						
				for(int k = 0; k < this.conf.getListMOBJ().size(); k ++)
				{
					if(this.conf.getListMOBJ().get(k).getName().equals(endereco))
					{
						this.conf.getListMOBJ().get(k).setSensorValue(temp_name, valor);
						break;
					}
				}					
			}		
	}
	
}
