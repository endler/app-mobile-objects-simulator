package mobj;

import java.util.List;
import java.util.ArrayList;
import java.util.UUID;
/*Simulated Mobile Objects class. */
public class MOBJ {

	
	/*
	 * Parameters:
	 * name: Name of the mobile object
	 * ListSensor: List of Sensors this MOBJ currently has*/
	private String name;		
	private List<Sensor> ListSensor;	

	public MOBJ(){
		this.name = UUID.randomUUID().toString().substring(0, 13);
		this.ListSensor = new ArrayList<Sensor>();
	}
	
	public MOBJ(String MAC){
		this.name = MAC;
		this.ListSensor = new ArrayList<Sensor>();
	}
	
	public String getName(){
		return this.name;
	}
	
	
	//Method used to add N (i) Sensors of the given Service Name (ServiceName) to this MOBJ 
	public void AddNSensors(Long i, double intervalo_l, double intervalo_u, String ServiceName, String SensorClass){
		int j;
		for(j=0; j<i; j++){
			Sensor sensor = new Sensor( intervalo_l,  intervalo_u, ServiceName, SensorClass);
			ListSensor.add(sensor);
		}
	}
	
	//Method used to get a specific sensor's data, given its index.
	public Double getSensorValue(int index){
		if(!this.ListSensor.isEmpty()){
			return(this.ListSensor.get(index).readValue());
		}
		return(0.0);
	}
	//Method used to get the Sensor's Service name.
	public String getSensorServiceName(int index){
		if(!this.ListSensor.isEmpty()){
			return(this.ListSensor.get(index).readName());
		}
	
		return("Null");
	}
	
	//Methods used to set the values of the MOBJs sensors.
	public void setRandomValuesALL(){
		int l = this.ListSensor.size();
		if(l>0){
			int i;
			for(i = 0; i < l; i++){
				this.ListSensor.get(i).writeValue();
			}
		}
	}
	
	
	public void setSensorValue(String name, Double mensagem){
		if(!this.ListSensor.isEmpty())
		{
			for(int k = 0; k < this.ListSensor.size(); k ++)
			{
				if(this.ListSensor.get(k).readName().equalsIgnoreCase(name))
				{
					this.ListSensor.get(k).writeValue(mensagem);
				}
			}
		}
	}
	
	public void setSensorValue(int index){
		if(!this.ListSensor.isEmpty()){
			this.ListSensor.get(index).writeValue();
		}
	}
	
	//Returns the total number of sensors from this MOBJ.
	public int listSensorSize(){
		return this.ListSensor.size();
	}
	
	

}
