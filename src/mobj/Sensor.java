package mobj;

import java.util.concurrent.ThreadLocalRandom;
/*Simulated Sensors class*/
public class Sensor {

	/*
	 * Parameters:
	 * name: Name of the Sensor's Service. e.g. Temperature
	 * value_d: Value of the sensor.
	 * Drange_low: The lower limit used to randomize the value of value_d.
	 * Drange_up: The upper limit used to randomize the value of value_d.
	 * */
	private String name;
	private double value_d, Drange_low, Drange_up;
	private int type;

	/*Sensor types:
	 * 1 - Sensor Only
	 * 2 - Actuator Only
	 * 3 - Sensor & Actuator
	*/
	public Sensor(double range_low, double range_up, String name, String _Class){
		this.name = name;
		double up, down;
		if(range_up > range_low){
			up = range_up;
			down = range_low;
		}
		else{
			up = range_low;
			down = range_up;
		}
		
		this.Drange_low = down;
		this.Drange_up = up;
		
		if(_Class.equalsIgnoreCase("Sensor")){this.type = 1;}
		else if(_Class.equalsIgnoreCase("Actuator")){this.type = 2;}
		else{this.type = 3;}
	}
	
	public String readName(){
		return(this.name);
	}
	
	public String readClass(){
		switch(this.type){
		case 1: return("Sensor");
		case 2: return("Actuator");
		case 3: return("Both");
		}
		return "";
	}
	
	public Double readValue(){	
			return(this.value_d);
		
	}
	
	public void writeValue(){
		if(this.type != 2){
			this.value_d = ThreadLocalRandom.current().nextDouble(this.Drange_low, this.Drange_up);}
		
	}
	
	public void writeValue(Double value){
		if(this.type != 1){
		this.value_d = value;}
		
		
	}
	
}
