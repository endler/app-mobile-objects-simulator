# README #

### What is this repository for? ###

App que simula Smart Objects para Mobile Hub. O funcionamento no Android 7.0 ou mais recente não é garantida.

### How do I get set up? ###

1) Abra o projeto SimuladorMO no Eclipse.
2) Execute a main do SimuladorMO, que irá criar o socket que sera utilizado na comunicação do simulador com o mobilehub.
3) Abra o projeto do mobilehub modificado no Android Studio e execute o mobilehub em um dispositivo Android Virtual (Testado para Android API 23 - Android 6.0).
4.1)Marque a checkbox "Simulator" no mobilehub executando.
4.2)Digite o endereço IP da máquina que está executando o simuladorMO no campo "IP Simulator" (Se estiver usando uma máquina virtual Android do Android Studio, use "10.0.2.2")
5)Clique no botão de "Play" para começar a simulação. Os objetos simulados podem ser vistos no Android Monitor do Android Studio ou na tela Viewer do mobilehub.
6)Para terminar a simulação, clique no botão de Stop.
	OBS:A execução do mobilehub com o checkbox "Simulator" marcado e sem a execução do SimuladorMO com o Socket aberto irá resultar em um crash do mobilehub.

Instruções para mudança de parâmetros dos Mobile Objects (M-Objs) simulados:
Os objetos a serem simulados são entradas em um JSON no arquivo Configs.txt
1) Abra o projeto SimuladorMO no Eclipse.
2) Abra o arquivo "Configs.txt" dentro do projeto para editar os tipos e quantidades de objetos a serem simulados.
3)Cada entrada no array "Set" é um objeto JSON que deve ter os seguintes parâmetros (3.1 a 3.4):
3.1) O parâmetro "MOBJS" indica quantos mobile objects simulados (número inteiro) dessa categoria o simulador criará.
3.4)"ServiceName" é um array com o nome do serviço oferecido pelos sensores (ex: Temperatura, Pressão, Humidade).
	(Ex: "ServiceName":["Temperatura","Luminosidade"] 
		Teremos um M-Obj que simulará serviços de Temperatura e Luminosidade)
3.3) "Sensor" é um array que indica a quantidade de sensores de cada serviço do mobile object. 
	(Ex:"Sensor":[1,2], "ServiceName":["Temperatura","Luminosidade"] 
	Teremos um M-Obj tem um sensor de temperatura e dois de luminosidade).
3.2) "LowerBound" e "UpperBound" são arrays dos limites do intervalo de valores que cada serviço do mobile object simularão (Os valores sempre serão do tipo Double); 
		(ex: Ex:"Sensor":[1,2], "ServiceName":["Temperatura","Luminosidade"], LowerBound = [0.0, 20.0], UpperBound = [10.0, 30.0] 
		Teremos um M-Obj com um sensor de temperatura, que varia de 0.0 a 10.0, e dois sensores de Luminosidade, que variam de 20.0 a 30.0
		
4) "ErrorChance" é o parâmetro que indica a probabilidade de um Scan de um mobile object falhar, em porcentagem. 
	Ex: ' "ErrorChance":10 ' indica que TODO mobile object simulado tem uma chance de 10% de se desconectar aleatóriamente
	e não enviar as informações ao mobile hub.
	
5)"Time": Intervalo de tempo (em segundos) em que o simulador deverá esperar para gerar valores novos aos objetos simulados. Esse parâmetro existe 
	para evitar que, em um intervalo pequeno de tempo, dois mobile hubs conectados a um mesmo simulador alterem os valores dos objetos simulados.
	Ex: Se Time = 40s, mas o parâmetro de tempo de Scan no mobile Hub é de 5s em dois mobile hubs diferentes executando ao mesmo tempo, eles 
	irão "escanear" os objetos simulados a cada 5 segundos. Se não "bloqueássemos" a geração de novos valores aos sensores, logo após um scan do 
	primeiro M-Hub, o segundo M-Hub iria atualizar os objetos simulados novamente. Portanto, os valores exibidos nos dois M-Hubs  seriam sempre diferentes.

6) Salve as mudanças feitas em Configs.txt e execute a main.

Exemplo adicional de Configuração:

{
	"ErrorChance":10,
	"Time":10,
	"Set":[{"MOBJS":2, "LowerBound":[10.0, 40.0, 90.0], "UpperBound":[30.0, 60.0, 110.0], "Sensor":[1,3,2], "ServiceName":["Temperatura", "Pressao", "Umidade"]},
	{"MOBJS":1, "LowerBound":[10.0], "UpperBound":[15.0], "Sensor":[1], "ServiceName":["Luminosidade"]},
	{"MOBJS":3, "LowerBound":[0.0, 0.0], "UpperBound":[15.0, 2.0], "Sensor":[1,1], "ServiceName":["Luminosidade", "Tensão"]}]
	
	Com essa configuração, teremos:
	10% de chance de "falhar" ao scannear um M-Obj, desconectando-o
	Pelo menos 10s deverão ter passado para que um request de scan enviado pelo Mobile Hub mude os valores do M-Objs simulados
	2 Objetos, com serviços de Temperatura, que contém um sensor variando de 10.0 a 30.0, Pressao, que contém 3 sensores variando 
		de 40.0 a 60.0, e Umidade, que contém 2 sensores variando de 90.0 a 110.0
	1 Objeto, com serviço de Luminosidade, com um sensor variando de 10.0 a 15.0
	3 Objetos, com serviços de Luminosidade, com 1 sensor variando de 0.0 a 15.0, e de Tensão, com 1 sensor variando de 0.0 a 2.0
}


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###


Simulador de Mobile Objects para Mobile Hub.
Desenvolvido por:
	Igor Kevin de Aguiar van Bentum (igor_kevin@hotmail.com)
	Pedro Tarragó Pinho (ptarragopinho@yahoo.com.br)



